To Install and run the project:

- Move to Backend Directory
- Execute `npm install`
- Execute `npm start`
- Open `http://localhost:3000`


# API

## General

#### Auth

```
    USER_ID:    ID
    USER_TOKEN: TOKEN 
```

### Return
 
```json
{
    "code": Int,
    "message": String,
    "data": JSON
}
```

### Return Codes

Code        | Value
---         | --- 
100         | Missing Arguments
101         | Missing POST argument
102         | Missing GET argument
110         | Bad Credentials
111         | Unknown user
112         | Invalid token
113         | Ressource not available for user
200         | Success
404         | Ressource not found
500         | Internal error
600         | Ressource failure
610         | Login failure
611         | Bad Credentials
620         | User failure
621         | Email invalid
622         | User mail exists
623         | User pseudo exists
624         | User not found
625         | Password Required
630         | Game failure
631         | Game exist


## LOGIN

- POST
  - Send
    ```json
    {
        "mail": String,
        "pass": String
    }
    ```
  - Return
    ```json
    { 
        "id": Int,
        "token": String
    }
    ```  


## User

### JSON
```json
{
    "id": Int, (Get)
    "pseudo": String,
    "email": String, (Opt)
    "password": String, (Set) (Opt)
    "platforms": [Platform], (Opt)
    "games": [Game], (Opt)
}
```

### Requests

- GET 
  - List
  - by Id __(Auth)__
  - by Mail __(Auth)__
  - by Pseudo
- POST
  - Empty (Create)
  - by Id (Update) __(Auth)__
- DELETE
  - by Id __(Auth)__

## Game

### JSON
```json
{
    "id": Int, (Get)
    "igdbId": Int, (Get)
    "name": String,
    "desc": String, (Get) (Opt)
    "ageRating": String, (Get) (Opt)
    "cover": String, (Get) (Opt)
    "releaseDate": Date, (Get) (Opt)
    "platforms": [Platform], (Get) (Opt)
}
```

### Requests

- GET 
  - List
  - by Id
  - by Name
  
## Platform

### JSON
```json
{
    "id": Int, (Get)
    "igdbId": Int, (Get)
    "name": String,
    "logo": String, (Get) (Opt)
    "releaseDate": Date, (Get) (Opt)
    "games": [Game], (Get) (Opt)
}
```

### Requests

- GET 
  - List
  - by Id
  - by Name