// SERVER -> localhost:3001
// USER_MAIL
// USER_PASS -> Pass en clair

// USER_TOKEN -> Mis a jours par le script
// USER_ID -> Mis a jours par le script

let server = pm.environment.get("SERVER");
let mail = pm.environment.get("USER_MAIL");
let pass = pm.environment.get("USER_PASS");

const cypher = {
  url: server + "/pass/" + pass,
  method: 'GET',
  header: {'Content-Type': 'application/json'},
  body: {} 
};

pm.sendRequest(cypher, function (err, response) { 
    let cypheredPass = response.json().data.hash
    
    const login = {
      url: server + "/login",
      method: 'POST',
      header: {'Content-Type': 'application/json'},
      body: {
        mode: 'raw',
        raw: JSON.stringify({ email: mail, pass: cypheredPass })
        }
    }
    pm.sendRequest(login, function (err, response) {
        let res = response.json();
        let token = res.data.token;
        let id = res.data.id;
        pm.environment.set("USER_TOKEN", token);
        pm.environment.set("USER_ID", id);
    });
    
})