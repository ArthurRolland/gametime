var express = require('express')
var path = require('path')
var logger = require('morgan')
let helper = require('./scripts/helper')

var app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'public')))

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*") 
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, user_id, user_token")
    next()
})

app.use('/', require('./routes/index'))
app.use('/pass', require('./routes/pass'))
app.use('/login', require('./routes/login'))
app.use('/user', require('./routes/user'))
app.use('/game', require('./routes/game'))
app.use('/platform', require('./routes/platform'))

// catch 404 and forward to error handler
app.use((req, res, next) => { helper.sendResponse(res, null, 404,  error.messageFor(404)) })
// error handler
app.use((err, req, res, next) => { helper.sendResponse(res, null, err.status || 500, err.message) })

module.exports = app
