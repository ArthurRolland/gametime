var express = require('express')
var router = express.Router()
let helper = require('../scripts/helper')
let db = require('../scripts/Database')

const emailRegEx = /\S+@\S+\.\S+/



router.route('/')
.get((req, res, next) => {
  /* GET users listing. */
  db.executeQuerry('SELECT id, pseudo FROM Player', (users) => {
    helper.sendResponse(res, users)
  })
})
.post((req, res, next) => {
  /* create user. */
  let body = req.body
  if (body.email && body.pseudo && body.pass) {
    let email = body.email
    let pass = body.pass
    let pseudo = body.pseudo
    if (emailRegEx.test(email)) {
      db.executeQuerry("SELECT id FROM Player WHERE email='" + email + "'", (results) => {
        if (results.length == 0) {
          db.executeQuerry("SELECT id FROM Player WHERE pseudo='" + pseudo + "'", (results2) => {
            if (results2.length == 0) {
              let hash = helper.hashPassword(pass, email)
              db.executeQuerry("INSERT INTO Player (pseudo, email, pass) VALUES ('" + pseudo + "', '" + email + "', '" + hash + "')", (result3) => { 
                helper.sendResponse(res, {})
              })
            }
            else { helper.sendResponse(res, null, 623) }
          })
        }
        else { helper.sendResponse(res, null, 622) }
      })
    }
    else { helper.sendResponse(res, null, 621)}
  }
  else { helper.sendResponse(res, null, 100)}
})

router.param('param', (req, res, next, param) => {
  let sql = "SELECT id, pseudo FROM Player WHERE pseudo=" + param
  if (param == parseInt(param)) { sql = "SELECT id, pseudo, email FROM Player WHERE id=" + parseInt(param) }
  else if (emailRegEx.test(param)) {   sql = "SELECT id, pseudo, email, pass FROM Player WHERE email=" + param}
  
  db.executeQuerry(sql, (results) => {
    req.results = results
    next()
  })
})

router.route('/:param')
.all((req, res, next) => {
  helper.auth(req, res, next)
})
.get((req, res, next) => {
  helper.sendResponse(res, req.results)
})
.post((req, res, next) => {
  let body = req.body
  if (body.email || body.pass || body.pseudo) {
    if (req.results.length == 1) {
      // We have an user and something to change
      let user = req.results[0]
      let headers = req.headers
      if (user.id == headers.user_id) {
        // User is not trying to change another account
        let lastCompletion = (args) => {
          let sql = "UPDATE Player SET " + args.join(", ") + " WHERE id=" +  user.id
          db.executeQuerry(sql, (r) => { helper.sendResponse(res, {})}) 
        }
        let passCompletion = (args, email, pass, nextCompletion) => {
          let hash = helper.hashPassword(pass, email)
          args.push("pass='" + hash + "'")
          nextCompletion(args) 
        }
        let emailCompletion = (args, email, pass, nextCompletion) => {
          args.push("email='" + email + "'")
          passCompletion(args, email, pass, nextCompletion)
        }
        let checkEmailAndPass = (args, body, passCompletion, lastCompletion) => {
          if (body.email) {
            if (body.pass) {
              db.executeQuerry("SELECT id FROM Player WHERE email='" + body.email + "' and id!=" + user.id, (results) => {
                if (results.length == 0) {
                  emailCompletion(args, body.email, body.pass, lastCompletion)
                } 
                else { helper.sendResponse(res, null, 622) }
              })
            }
            else { helper.sendResponse(res, null, 625) }
          }
          else if (body.pass) {
            passCompletion(args, user.email, body.pass, lastCompletion)
          }
          else { lastCompletion(args) }
        }

        let baseArgs = []
        if (body.pseudo) {
          db.executeQuerry("SELECT id FROM Player WHERE pseudo='" + body.pseudo + "' and id!=" + user.id, (results) => {
            if (results.length == 0) {
              baseArgs.push("pseudo='" + body.pseudo + "'")
              checkEmailAndPass(baseArgs, body, passCompletion, lastCompletion)
            } 
            else { helper.sendResponse(res, null, 623) }
          })
        }
        else {
          checkEmailAndPass(baseArgs, body, passCompletion, lastCompletion)
        }
      }
      else { helper.sendResponse(res, null, 113)}
    }
    else { helper.sendResponse(res, null, 624)}
  }
  else { helper.sendResponse(res, null, 101) }
})
.delete((req, res, next) => {
  helper.sendResponse(res, req.user)
})

module.exports = router
