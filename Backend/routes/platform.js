var express = require('express')
var router = express.Router()

let helper = require('../scripts/helper')
let db = require('../scripts/Database')

/* GET platform listing. */
router.route('/')
.all((req, res, next) => {
  helper.auth(req, res, next)
})
.get((req, res, next) => {
  db.executeQuerry('SELECT id, name FROM Platform', response => {
    helper.sendResponse(res, response)
  })
})

router.param('param', (req, res, next, param) => {
  if (param == parseInt(param)) {
    let id = param
    db.executeQuerry("SELECT * FROM Platform WHERE id = " + id, response => {
      if (response.length == 1) {
        req.results = response[0]
        db.executeQuerry('SELECT idGame FROM GameOnPlatform WHERE idPlatform = ' + id, response2 => {
          req.results.games = response2.map( it => it.idGame )
          next()
        })
      }
      else {
        req.results = []
        next()
      }
    })
  }
  else {
    req.results = []
    next()
  }
})

router.route('/:param')
.all((req, res, next) => {
  helper.auth(req, res, next)
})
.get((req, res, next) => {
  helper.sendResponse(res, req.results)
})

module.exports = router
