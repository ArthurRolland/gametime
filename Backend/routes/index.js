var express = require('express');
var router = express.Router();
let helper = require('./../scripts/helper')

/* GET home page. */
router.get('/', function(req, res, next) { 
  helper.sendResponse(res) 
});

module.exports = router;
