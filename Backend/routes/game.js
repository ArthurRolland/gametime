var express = require('express')
var router = express.Router()
let axios = require('axios')

let helper = require('../scripts/helper')
let db = require('../scripts/Database')

const emailRegEx = /\S+@\S+\.\S+/

router.route('/')
.all((req, res, next) => {
  helper.auth(req, res, next)
})
.get((req, res, next) => {
  /* GET users listing. */
  db.executeQuerry('SELECT id, name, igdbId FROM Game', (games) => {
    helper.sendResponse(res, games)
  })
})
.post((req, res, next) => {
  /* create Game. */
  let body = req.body
  if (body.igdbId) {
    let igdbId = body.igdbId
    db.executeQuerry("SELECT id FROM Game WHERE igdbId=" + igdbId, (results) => {
      if (results.length == 0) {
        addGame(igdbId, game => helper.sendResponse(res, game))
      }
      else { helper.sendResponse(res, null, 631) }
    })
  }
  else { helper.sendResponse(res, null, 100)}
})


router.param('param', (req, res, next, param) => {
  if (param == parseInt(param)) { 
    let sql = "SELECT * FROM Game WHERE id=" + parseInt(param)
    db.executeQuerry(sql, (games) => {
      if (games.length > 0) {
        let game = games[0]
        let pSql = "SELECT idPlatform FROM GameOnPlatform WHERE idGame=" + game.id
        db.executeQuerry(pSql, (platforms) => {
          game.platforms = platforms.map((platform) => platform.idPlatform )
          req.results = game
          next()
        })
      }
      else {
        req.results = []
        next()
      }
    })
  }
  else { 
    let sql = "SELECT id, name, igdbId FROM Game WHERE name like '%" + param + "%'" 
    db.executeQuerry(sql, (results) => {
      let games = { gameTime: results }
      axios({
        url: "https://api-v3.igdb.com/games",
        method: 'POST',
        headers: helper.igdbHeader(),
        data: 'fields name; search "' + param + '"; limit 100;'
      })
      .then(response => {
        let ids = results.map(el => el.igdbId) 
        games.igdb = response.data.filter( el => ids.indexOf(el.id) == -1) 
        req.results = games
        next()
      })
      .catch(err => {
        console.log(err)
        req.results = games
        next()
      })
    })
  }
})

router.route('/:param')
.all((req, res, next) => {
  helper.auth(req, res, next)
})
.get((req, res, next) => {
  helper.sendResponse(res, req.results)
})
.delete((req, res, next) => {
  helper.sendResponse(res, req.user)
})

function addGame(igdbId, completion) {
  let headers = helper.igdbHeader()
  let info = { igdbId: igdbId }
  axios({
    url: "https://api-v3.igdb.com/games",
    method: 'POST',
    headers: headers,
    data: 'fields name, first_release_date, age_ratings, platforms, summary, cover; where id = ' + igdbId + ';'
  })
  .then(response => {
    if (response.data.length == 1) {
      info.game = response.data[0]
      info.desc = info.game.summary
      info.name = info.game.name
      let timestamp = info.game.first_release_date
      let date = new Date(timestamp * 1000)
      info.release = date.toISOString().slice(0, 19).replace('T', ' ')
      
      if (info.game.age_ratings != undefined) {
        let rating = ""
        if (info.game.age_ratings.length > 1) { rating = info.game.age_ratings[1] }
        else { rating = info.game.age_ratings[0] }
        return axios({
          url: "https://api-v3.igdb.com/age_ratings",
          method: 'POST',
          headers: headers,
          data: 'fields category, rating; where id = ' + rating + ';'
        })
      }
      return
    }
    return 
  })
  .then(response => {
    console.log(response)
    if (response) { info.age = ratingFor(response.data[0]) }
    else { info.age = "NA" }
    return axios({
      url: "https://api-v3.igdb.com/covers",
      method: 'POST',
      headers: headers,
      data: 'fields url; where id = ' + info.game.cover + ';'
    })
  })
  .then(response => {
    info.cover = "http:" + response.data[0].url
    return 
  })
  .catch(err => {
    console.log(err)
    info = { error: 622 }
    return
  })
  .then(response => { 
    if (info.cover != undefined && info.name != undefined && info.age != undefined) {
      let sql = "INSERT INTO Game (name, igdbId, age, \`desc\`, cover, \`release\`) VALUES ('" + db.sanitize(info.name) + "', " + info.igdbId + ", '" + info.age + "', '" + db.sanitize(info.desc) + "', '" + info.cover + "', '" + info.release + "')"
      db.executeQuerry(sql, resp => {
        db.executeQuerry("SELECT id FROM Game WHERE igdbId=" + info.igdbId, res2 => {
          if (res2.length == 1) {
            info.id = res2[0].id
            let platforms = info.game.platforms
            info.game = undefined
            setPlatformsForGame(info, platforms, res => {
              info.platforms = res
              completion(info)
            })
          }
          else { completion({error: 623}) }
        })
      })
    }
  })
}


function setPlatformsForGame(game, platforms, completion) {

  function setPlatform(igdbId, repeat, next) {
    let pID = null
    db.executeQuerry("SELECT id FROM Platform WHERE igdbId = " + igdbId, res => {
      if (res.length == 1) {
        pID = res[0].id
        db.executeQuerry("INSERT INTO GameOnPlatform (idGame, idPlatform) VALUES (" + game.id + ", " + pID + ")", res => {
          next(pID)
        })
      }
      else if (repeat){
        helper.updatePlatform(() => {
          setPlatform(igdbId, false, next)
        })
      }
    })
  }

  let count = 0
  let ids = []
  function setNextPlatform(end) {
    setPlatform(platforms[count], true, (id) => {
      ids.push(id)
      count++
      if (count < platforms.length) { setNextPlatform(end)  }
      else { end(ids) }
    })
  }

  setNextPlatform(completion)
}

function ratingFor(rating) {
  let catEnum = ["ESRB", "PEGI"]
  let ratingEnum = ["3", "7", "12", "16", "18", "RP", "EC", "E", "E10", "T", "M", "AO"]
  return catEnum[rating.category-1] + "-" + ratingEnum[rating.rating-1]
}
module.exports = router
