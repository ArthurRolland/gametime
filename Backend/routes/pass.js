var express = require('express')
var router = express.Router()
let helper = require('./../scripts/helper')


router.get('/', (req, res, next) => {
  helper.sendResponse(res, null, 4012, "No pass provided")
})

router.param('pass', (req, res, next, pass) => {
  req.pass = pass
  next()
})

router.get('/:pass', (req, res, next) => {
  helper.sendResponse(res, { pass: req.pass, hash: helper.sha256(req.pass) })
})
module.exports = router