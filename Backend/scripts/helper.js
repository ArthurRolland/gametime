let crypto = require('crypto')
const db = require("./Database")
let error = require('./Error')
let axios = require('axios')

const SALT = "dPOZb9SLFTGxGeRG9zQU5gaRY1lan/34VIOeY7evCag="
class Helper {
    auth(req, res, next) {
        let headers = req.headers
        if (headers.user_token != undefined && headers.user_id != undefined){
            let id = parseInt(headers.user_id)
            if (isNaN(id)) { id = 0 }
            let token = headers.user_token
            db.executeQuerry("SELECT id, token FROM Player WHERE id=" + id, (results) => {
                if (results.length == 1) {
                    let user = results[0]
                    if (user.token == token) { next() }
                    else { this.sendResponse(res, null, 112)}
                }
                else { this.sendResponse(res, null, 111)}
            })
        }
        else { this.sendResponse(res, null, 110)}
    }

    generateToken() {
        let str = ""
        for (let i = 0; i < 64; i++) {
            let r = this.randomChar()
            if (Math.random() > 0.5) { r = r.toUpperCase() }
            str += r
        }
        // ? Changed
        return this.sha256(str)
    }

    hashPassword(pass, mail) {
        return this.sha256(pass + SALT + this.sha256(mail))
    }

    sha256(message) {
        const hash = crypto.createHash('sha256')
        hash.write(message)
        // ? Changed
        return hash.digest('hex')
    }

    randomChar() {
        const alphabet = "qwertyuiopasdfghjklzxcvbnm1234567890"
        let r = Math.floor(Math.random() * alphabet.length)
        return alphabet[r]
    }

    sendResponse(res, payload, code, message) {
        console.log("sending response")
        console.log({payload, code, message})

        if (payload == undefined) {
            payload = null
            if (code == undefined) {
                code = 500
            }

            if (message == undefined) {
                message = error.messageFor(code)
            }
        }
        else if (payload != null){
            code = 200
            message = "Success"
        }

        res.send({
            code: code,
            status: message,
            data: payload
        })
        res.end()
    }

    igdbHeader() {
        return {
            'Accept': 'application/json',
            'user-key': '1929e9dfcba66ea56c8f6b9dcc291e43'
        }
    }

    updatePlatform(completion) {
        let headers = this.igdbHeader()
        axios({
            url: "https://api-v3.igdb.com/platforms",
            method: 'POST',
            headers: headers,
            data: 'fields name, platform_logo.url; limit 500; sort id asc;'
        })
        .then(response => {
            if (response.data.length > 0) {
                let count = 0
                function endUpdate(){ if (count == response.data.length) { completion(response.data)  } }
                for (const platform of response.data) {
                    let logo = ""
                    if (platform.platform_logo != undefined) { logo = "http:" + platform.platform_logo.url }
                    db.executeQuerry('INSERT INTO Platform (igdbId, name, logo) VALUES (' + platform.id + ', "' + platform.name + '", "' + logo + '")', resp => {
                        count++
                        endUpdate()
                    })
                }
            }
        })
        .catch(error => {
            console.log("Request error: " + error)
            completion([])
        })
    }

}

module.exports = new Helper()

