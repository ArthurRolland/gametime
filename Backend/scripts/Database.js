const mysql = require('mysql')

class Database {
    constructor() {
        this.connection = mysql.createConnection({
            host: "localhost",
            user: "root",
            port: 8889,
            password: "root",
            database: "GameTime"
        })
        this.connection.connect(function(err) { if (err) { throw err } })
    }

    executeQuerry(sql, completion) {
        this.connection.query(sql, (err, result, fields) => {
            if (err) { 
                completion([])
            }
            else { 
                let json = []
                if (result != undefined && result != null && result != "") {
                    try { json = JSON.parse(JSON.stringify(result)) }
                    catch (error) { console.log("Error parsing request result: " + error) }
                }
                completion(json) 
            }
        })
    }
    
    sanitize(str) {
        return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
            switch (char) {
                case "\0":
                    return "\\0";
                case "\x08":
                    return "\\b";
                case "\x09":
                    return "\\t";
                case "\x1a":
                    return "\\z";
                case "\n":
                    return "\\n";
                case "\r":
                    return "\\r";
                case "\"":
                case "'":
                case "\\":
                case "%":
                    return "\\"+char; // prepends a backslash to backslash, percent,
                                        // and double/single quotes
                default:
                    return char;
            }
        })
    }
}

module.exports = new Database()