class Error {
    constructor() {
        this.errors = {
            // General arguments
            "100": "Missing Arguments",
            "101": "Missing POST argument",
            "102": "Missing GET argument",
            // Auth error
            "110": "Bad Credentials",
            "111": "Unknown user",
            "112": "Invalid token",
            "113": "Ressource not available for user",
            // Success
            "200": "Success",
            // HTTP basics
            "404": "Ressource not found",
            "500": "Internal error",
            // Ressources
            "600": "Ressource failure",
            // - Login failure
            "610": "Login failure",
            "611": "Bad Credentials",
            // User failure
            "620": "User failure",
            "621": "Email invalid",
            "622": "User mail exists",
            "623": "User pseudo exists",
            "624": "User not found",
            "625" : "Password Required",
            // Game failure
            "630": "Game failure",
            "631": "Game exist",
        }
    }

    messageFor(code) { return this.errors[parseInt(code)] || "UNKNOWN ERROR" }
}

module.exports = new Error()